# R3.school

## June 14th, 2022

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br><br>
    <h1>Trilogy of git - Part III</h1>
    <h2>Contribute using the command line</h2>
    <br><br><br>
    <h4>
        Laurent Heirendt, Ph.D.<br>
        R3 Team - <a href="mailto:lcsb-r3@uni.lu">lcsb-r3@uni.lu</a><br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>
