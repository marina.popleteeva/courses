# Data and metadata
<div style="display:grid;grid-gap:100px;grid-template-columns: 40% 40%">
<div >

## Data
  * "*information in digital form that can be transmitted or processed*"
  <p align="right">-- Merriam-Webster dictionary</p>
  * "*information in an electronic form that can be stored and processed by a computer*"
  <p align="right">--Cambridge dictionary</p>

</div>
<div>

## Metadata
  * data describing other data
  * information that is given to describe or help you use other information
  * metadata are data
    * can be processed and analyzed
</div>
</div>

<div class="fragment">

## Metadata examples:
<div style="position:absolute">
  <ul>
    <li> LabBook </li>
    <li> author/owner of the data</li>
    <li> origin of the data 
    <li> data type
  </ul>
</div>
<div style="position:absolute;left:25%">
  <ul>
    <li> description of content </li>
    <li> modification date </li>
    <li> description of modification </li>
    <li> location </li>
  </ul>
</div> 
<div style="position:relative;left:50%;top:0.7em">
  <ul> 
    <li> calibration readings</li>
    <li> software/firmware version</li>
    <li> data purpose</li>
    <li> means of creation</li>
  </ul>
</div>
</div>

<div class="fragment">
<br>
</center>
<center style="color:red">!Insufficient metadata make the data useless!</center>
</div>
<aside class="notes">
Sometimes metadata collection takes more time than data collection
</aside>



# LCSB research data
three categories:
  * **Primary data** 
    * scientific data 
      * measurements, images, observations, notes, surveys, ...
      * models, software codes, libraries, ...
    * metadata directly describing the data
      * data dictionaries
      * format, version, coverage descriptions, ...

  * **Research record**
    * description of the research process, including experiment 
      * experiment set-up
      * followed protocols
      * ...
    
  * **Project accompanying documentation** 
      * ethical approvals, information on the consent
      * collaboration agreements
      * intellectual property ownership
      * other relevant documentation

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right;">
<a href="https://howto.lcsb.uni.lu/internal/policies/LCSB-POL-BIC-03/" style="color:grey; font-size:0.8em;">LCSB-POL-BIC-03 Research Data Retention and Archival Policy</a>
</div>