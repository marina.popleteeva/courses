# Physical Security

<div >

 "<center>*Physical security describes security measures that are designed to deny unauthorized access to facilities, equipment and resources and to protect personnel and property from damage or harm (such as espionage, theft, or terrorist attacks)* </center>"

<center> <img height="230px" src="slides/img/physical_security.jpg"> </center>

<div style="position:absolute;top:30%;left:2%">

## LCSB offices 

  * Rouden Eck offices are locked by default
  * Technical measures exist to individually control access to the building
  * Physical access is limited to minimal authorized personnel
  * Return of access badge is required when personnel contract is terminated
  * Access to the data center requires approval(CIO)
  * Visitors and external personnel acesss are monitoring
  
  

</div>

<div style="position:relative;top:80%;left:60%">

## Home Office, new security challenges

 * Separate your work life from your home life 
 * Secure your home office
 * Secure your home router
 * Use VPN to access university applications.
 * Encrypt your devices
 * Keep your operating systems up to date
 * Enable Automatic locking
  
</div>
</div>
