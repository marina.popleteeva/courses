# Introduction
<div  class="fragment" style="position:absolute">

## Learning objectives

  * How to manage your data
  * How to look and analyze your data
  * Reproduciblity in the research data life cycle
</div>

<div  class="fragment" style="position:relative;top:80%;left:60%">

## Pertains to practically all people at LCSB
   * Scientists
   * PhD candidates
   * Technicians
   * Administrators
</div>

<center>
<img   height="450px" src="slides/img/wordcloud.png"><br>
</center>
