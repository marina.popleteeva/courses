
# Some practical recommendations

* Do your data processing according to the data life cycle steps

<div class='fragment' style="position:relative;left:25%;top:60%">
<img  align="middle" height="300px" src="slides/img/rdm-cycle.png">
</div>
<div class="fragment">

* Use data management planning tools:
  * DMPonline <a href="https://dmponline.elixir-luxembourg.org/" style="color:blue; font-size:0.8em;">https://dmponline.elixir-luxembourg.org/</a>
   <img  src="slides/img/dmponline_logo.png" height="50px">
  * DS Wizard <a href="https://learning.ds-wizard.org/" style="color:blue; font-size:0.8em;">https://learning.ds-wizard.org/</a> 
  <img  src="slides/img/dsw_logo.png" height="100px">
  
<div>

<div class="fragment">

* Annotate your data and code using Ontologies and standardised metadata

</div>
