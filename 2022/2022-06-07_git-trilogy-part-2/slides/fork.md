# Fork the upstream repository


If you recently contributed to the upstream repository, please delete the fork and start over (`Settings` > `General` > `Advanced` > `Delete project`)

<div class="fragment">
<br>
If you do not have write access to the `upstream` repository, you have to fork it (as seen in the previous course):

- make it `public` or `private` depending on upstream repository
- select your namespace

<center>
<img width="50%" src="slides/img/fork.png">
</center>

<img src="slides/img/icon-live-demo.png" height="100px"> Go to [https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages](https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages) and fork.