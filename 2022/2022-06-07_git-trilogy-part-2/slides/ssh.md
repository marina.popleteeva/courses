# Key-based authentication

When communicating with the Gitlab server, you need credentials (username and password). Disadvantage: you have to enter the password every time.

Better: identify your laptop once, and then you do not need to enter the credentials anymore.

You need to do this setup just once (and repeat if you change your computer).<br>
- Open VS code
- Browse to `Terminal` > `New Terminal`
- Type this command at the command prompt: `ssh-keygen -t ed25519 -C firstname.lastname@uni.lu`
- Accept the suggested filename and directory (press `Enter`):
  ```
  Generating public/private ed25519 key pair.
  Enter file in which to save the key (/home/user/.ssh/id_ed25519):
  ```

- Press `Enter` (entering the passphrase is optional).



# Key-based authentication

A public and private key are generated and stored in specific location on your laptop. Now you need to add the public key to your GitLab account:

Copy the contents of your public key file:
- macOS: `tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy`
- Windows: `cat ~/.ssh/id_ed25519.pub | clip`

Now, let's move to GitLab https://gitlab.lcsb.uni.lu/

- On the top bar, in the top right corner, select your avatar, then `Preferences`.
<center>
<img height="200px" src="slides/img/addSSHavatar.png">
</center>



# Key-based authentication

- On the left side, select **SSH Keys**.
- In the **Key** box, paste the copied public key. Make sure it starts with *ssh-ed25519* and finishes with your e-mail.
- In the **Title** box, type a description, like *Work Laptop*.<br>
<center>
<img width="60%" src="slides/img/addSSH.png">
</center>



# Key-based authentication

Verify that you connect by typing in the Terminal of VS code:
```bash
ssh -p 8022 -T git@gitlab.lcsb.uni.lu
```

The first time, you have to enter `Yes` at the displayed question. Once successful, this should display something like:
```bash
$ ssh -p 8022 -T git@gitlab.lcsb.uni.lu
Welcome to GitLab, @firstname.lastname
```