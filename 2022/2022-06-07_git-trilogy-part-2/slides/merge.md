# Merge your changes to original repository via **Merge Request**

Now your changes are in your fork on the server.

If you want to incorporate your work into original (upstream) repository, submit a **Merge Request** via the Gitlab interface.

- If you just published branch, click "Create merge request"

<center>
<img width="30%" src="slides/img/mergePublishedBranch.png">
</center>

- If the button does not show up, go to `Repository > Branches` and click on `Merge request` for the branch you want to merge.

<center>
<img width="50%" src="slides/img/mergeRepoBranches.png">
</center>




# Merge your changes to original repository via **Merge Request**

- Make sure you are merging the *new* branch from **your fork** to the *develop* branch in **original repository**

<center>
<img height="130px" src="slides/img/MRwrong.png">
<img height="130px" src="slides/img/MRright.png">
</center>

- Give a meaningful title (start with "Draft:" if you are planning to continue work on this feature).
- Tick `Delete source branch when merge request is accepted.`

<center>
<img height="430px" src="slides/img/mergeCheck.png">
</center>