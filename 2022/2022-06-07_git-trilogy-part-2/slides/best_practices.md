# Best practices

* Work on your <font color="red">own</font> branch (in your own fork), and **not** on `master` and **not** on `develop`
* Do **not commit** to `master`/`develop`, but **submit a merge request (MR)**
* Get your code **reviewed** by your peers (submit a merge request!)
* Submit a merge request **often**! (as soon as the feature is ready)
* Make commits often (keeps increments small)
