# Git repository hosting platforms

<div style="position:absolute; left:70%">
<img src="https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo-extra-whitespace.png" alt="GitLab" style="width: 500px;"/>
</div>

Web-based platforms with **many** features facilitating storing, tracking and collaborating on `git` repositories.

* cloud based - everything on remote server
* authentication and authorization - who can see or access the repository and how
* access to files via browser - manage, view, make changes, ...
* collaboration supporting features - comment, create issues, documentation, ...

**Gitlab** is both publicly available or can be deployed on-premise.

- Public GitLab: [https://gitlab.com](https://gitlab.com)
- **LCSB** specific: [https://gitlab.lcsb.uni.lu](https://gitlab.lcsb.uni.lu)

Other popular platforms:
- **GitHub**: [https://github.com](https://github.com)
- **BitBucket**: [https://bitbucket.org](https://bitbucket.org)
- **Gitea**: [https://gitea.io](https://gitea.io)



# Login and report and issue

1. Login to [https://gitlab.lcsb.uni.lu](https://gitlab.lcsb.uni.lu)
   * use your LUMS credentials
2. Browse the existing repositories
   * Explore and Search
   * Navigate to the training repository [https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages](https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages)
3. Inspect project/repository folder structure
4. Report an issue

<img src="slides/img/icon-live-demo.png" height="100px">

