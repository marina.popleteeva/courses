# New term:  <font color="color:red">fork</font>
## What is a `fork`?

<center>
<img src="slides/img/fork.jpg" class="as-is" height="500em"/>
</center>



# New term:  <font color="color:red">fork</font>
## Not really ...

<center>
<img src="slides/img/fork-crossed.png" class="as-is" height="500em"/>
</center>



# New term:  <font color="color:red">fork</font>
## What is a `fork`?

- In general, by default, you can only **read** files in a repository. You **cannot write**.

<div class="fragment">
<div align="center">
<img src="slides/img/forking.png" width="80%">
</div>

- You have to work on your **own copy** of the repository in order to make changes.
- In other words, you have to work on your own <font color="red">**fork**</font>.




# How to get a fork?

Browse to the original repository and click on the button `Fork`:

<div align="center">
<img src="https://docs.gitlab.com/ee/user/project/repository/img/forking_workflow_fork_button_v13_10.png" height="150">
</div>

<br>

<img src="slides/img/icon-live-demo.png" height="100px">


## Time to practice!

1. Fork the training repository  [https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages](https://gitlab.lcsb.uni.lu/R3/school/git/basic-practice-pages)


Congrats! Now you have your own **fork**!



# How to update my fork?

As you have your own copy (fork), it will not automatically be updated once the original repository is updated.

![bulb](slides/img/bulb.png) You have to update it yourself!

<br>

**More on that later - time permitting**
