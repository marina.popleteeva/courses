# New term: <font color="color:red">merge</font>

* You have made changes on your branch
* Now you want your changes to be reflected on another branch </br>(most probably `develop`)

<div style="position:absolute;left:70%;top:1em">
<img src="slides/img/branch_merge-detail.png" height="800px" >
</div>

You have to <font color="color:red">merge</font> the branches.

- Merging is directional - *Merge branch A to branch B*
- Merging of branches is a process that is initiated via a so called **merge request** (MR):
- Your peers can comment on your changes in a merge request **review**!



# What happened in the background?

<div align="center">
<img src="slides/img/fork_branch-diagram-after-commit.png" height="600px" >
</div>



# What happened in the background?

<div align="center">
<img src="slides/img/fork_branch-diagram-after-commit-with-back-arrow.png" height="600px" >
</div>



# Merge request (MR)

<img src="slides/img/icon-live-demo.png" height="100px">

After hitting the *Commit* button, you get redirected to the submission page for your *merge request (MR)*

* Propose to merge your branch into the *development branch*. This is shown at the top of the page.
* Fill your merge request submission form and select an *Assignee*.
* Under Changes tab, you can see your changes

![bulb](slides/img/bulb.png) Good practice is to select *Delete source branch when merge request is accepted*. Why?
