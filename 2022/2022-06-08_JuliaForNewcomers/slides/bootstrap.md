<div class=leader>
<i class="twa twa-axe"></i><i class="twa twa-carpentry-saw"></i><i class="twa twa-screwdriver"></i><i class="twa twa-wrench"></i><i class="twa twa-hammer"></i><br>
Bootstrapping Julia
</div>



# Installing Julia

Recommended method:

- Download an archive from https://julialang.org/downloads/
- Execute `julia` or `julia.exe` as-is
- Link it to your `$PATH`

Distribution packages usually work well too:

- **Debians&Ubuntus**: `apt install julia`
- **Iris/Aion**: `module add lang/Julia`



# Life in REPL

```julia
user@pc $ julia

julia> sqrt(1+1)
1.4142135623730951

julia> println("Well hello there!")
Well hello there!

julia> ?
help?> sqrt

sqrt(x)

Computes the square root .....
```



# REPL modes

Julia interprets some additional keys to make our life easier:

- `?`: help mode
- `;`: shell mode
- `]`: packaging mode (looks like a box!)
- `Backspace`: quits special mode
- `Tab`: autocomplete anything
- `\`... `Tab`: expand math characters




# Managing packages from the package management environment

-   Install a package

```julia
] add UnicodePlots
```

-   Uninstall a package

```julia
] remove UnicodePlots
```



# Loading libraries, modules and packages

-   Load a local file (with shared functions etc.)

```julia
include("mylibrary.jl")
```

-   Load a package, add its exports to the global namespace

```julia
using UnicodePlots
```



# <i class="twa twa-light-bulb"> </i> How to write a standalone program?

*Your scripts should communicate well with the environment!*
(that means, among other, you)

```julia
#!/usr/bin/env julia

function process_file(filename)

  @info "Processing $filename..."

  # ... do something ...

  if error_detected
    @error "something terrible has happened"
    exit(1)
  end
end

for file in ARGS
    process_file(file)
end
```

Correct processing of commandline arguments makes your scripts *repurposable*
and *configurable*.




# <i class="twa twa-light-bulb"></i> Workflow: Make a local environment for your script


- Enter a local project with separate package versions

```julia
] activate path/to/project
```

- Install dependencies of the local project

```julia
] instantiate
```

- Execute a script with the project environment

```sh
$ julia --project=path/to/project script.jl
```

(Project data is stored in `Project.toml`, `Manifest.toml`.)
