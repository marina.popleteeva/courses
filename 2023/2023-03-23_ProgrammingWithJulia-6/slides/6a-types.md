
<div class=leader>
<i class="twa twa-locomotive"></i>
<i class="twa twa-helicopter"></i>
<i class="twa twa-police-car"></i>
<i class="twa twa-trolleybus"></i>
<br>
Data and their types
</div>



# What are types?

Types are a small domain-specific metalanguage that allows programs to *prove facts* about other programs

- originally from set theory (after Russel's disaster, preceding the Gödel's disaster)
- the classes of provable facts vary:
  - in λ-calculi, types are theorems that often uniquely determine code
  - in <i class="twa twa-snake"></i>, types are more like comments that do not prove anything
  - in many languages, types determine the format of data interfaces and data


Julia types:
- determine the format of data
- may carry additional information!
- may be materialized ("reified") and used at runtime



# Types in Julia

Primitive types: `Int32`, `UInt64`, `Float32`, `Char`, `Nothing`, `Function`, ...

Compound types:
```julia
struct FloatComplex
  re::Float64
  im::Float64
end
```



# Types in Julia

Parametrized types: `Tuple{...}`, `Array{...}`, `NTuple{N, ...}`, ...

Type aliases:
```julia
const Vector{X} = Array{X, 1}
```

Abstract types (organized with `<:`):
```julia
abstract type Anything end
struct Something <: Anything
  ...
end
```

`Union` and `UnionAll` types:
```julia
const Maybe{X} = Union{Nothing, X}
const VectorOfReals = (Vector{T} where {T<:Real})
```



# How does dispatch work?

Upon a function call, Julia selects the *least generic* method of the function
- the ordering is defined by `<:` on parameter types
- dispatch on multiple parameters is possible and (quite) efficient
  - this is the main difference from many other languages!
- dispatch on multiple parameters may be ambiguous!

```julia
julia> f(::Real, ::Int) = 1
f (generic function with 1 method)

julia> f(::Int, ::Real) = 2
f (generic function with 2 methods)

julia> f(1,2)
ERROR: MethodError: f(::Int64, ::Int64) is ambiguous.
```



# How does dispatch work?

Upon a function call, Julia selects the *least generic* method of the function.
- the call is specialized
- size of primitive types MAY be known (their data may be placed directly into registers or arrays)
- size of abstract types may NOT be known (their data must be *boxed*)

Compare:
- `sizeof(['a','b'])`, `sizeof([1.1f0, 2.2f0])`, `sizeof([1.1f0, 'b'])`
- function specialization possibilities:
  - `function f(x::Any)`
  - `function f(x::Int)`
  - `function f(x)`, which is internally `function f(x::T) where {T}`



# Exercise: let's make a ZOO

Zoo has:
- <i class="twa twa-elephant"></i> (with a numeric weight)
- <i class="twa twa-penguin"></i> (each with a name)
- <i class="twa twa-lion"></i> (each of which may be hungry or not)

These are hierarchically organized:
- there may be <i class="twa twa-soft-ice-cream"></i> stands
- some things are grouped at the same place
- there may be signposted <i class="twa twa-information"></i><i class="twa twa-up-right-arrow"></i> paths that lead to a farther thing
- some things are placed in <i class="twa twa-construction"></i>enclosures<i class="twa twa-construction"></i>

"You arrive at the zoo. There's a satiated lion and a small elephant, and also a signpost saying `penguins this way`. If you go that way, there are 5 penguins in an enclosure and an ice cream stand."



# Exercises: ZOO

Make functions that:
- count all penguins
- compute the weight of everything in the zoo
- make all animals in the zoo do their typical sound (`nudge(myZoo)`)
- find the most hidden animals in the zoo (they are behind the most signs)
- find all paths to hungry lions that are not in an enclosure
- print out a text description of the zoo
- convert a Julia datatype to your custom zoo datatype
  - `(:penguin, "El Jefe")`, `:hungry_lion`, `:satiated_lion`, `(:elephant, 50.0)`, `:ice_cream_stand`
  - groups of things are in a vector
  - signposted paths are made of Pairs (`"Penguins go here" => [(:penguin, ...), ....]`)
  - `throw` a good error if some data cannot be processed
