
<div class=leader>
<i class="twa twa-blue-circle"></i>
<i class="twa twa-red-circle"></i>
<i class="twa twa-green-circle"></i>
<i class="twa twa-purple-circle"></i><br>
Julia language primer (continued)
</div>



# Subroutines (a.k.a. functions)

- Multi-line function definition

```julia
function combine(a,b)
  return a + b
end
```

- "Mathematical" neater definition

```julia
combine(a,b) = a + b
```

- <i class="twa twa-light-bulb"></i> Definition with types specified (prevents errors, allows optimizations!)

```julia
function combine(a::Int, b::Int)::Int
    return a + b
end
```



# <i class="twa twa-light-bulb"></i><i class="twa twa-light-bulb"></i> Control flow: subroutine overloading (methods)

- A method for combining integers

```julia
combine(a::Int, b::Int)::Int = a + b
```

- A method of the "same function" for combining strings

```julia
combine(a::String, b::String)::String = "$a and $b"
```

- A method for combining vectors of integers:

```julia
combine(a::Vector{Int}, b::Vector{Int}) = a .* b
```

- A method for combining anything that "looks like a vector":

```julia
combine(a::AbstractVector, b::AbstractVector) = a + b
```



# <i class="twa twa-light-bulb"></i><i class="twa twa-light-bulb"></i><i class="twa twa-light-bulb"></i>  Supertype hierarchy

Types possess a single supertype, which allows you to easily group
multiple types under e.g. `Real`, `Function`, `Type`, `Any`, ...

This creates *groups of types* that are useful for restricting your functions to work on the most reasonable subsets of inputs.

```julia
julia> Int
Int64

julia> Int.super
Signed

julia> Int.super.super
Integer

julia> Int.super.super.super
Real

julia> Int.super.super.super.super
Number

julia> Int.super.super.super.super.super
Any
```

(Upon calling the function, Julia picks the *most specific* available method.)

<i class="twa twa-red-question-mark"></i> What are the subtypes of `AbstractVector`?



# Function arguments

-   Keyword arguments (can not be used for overloading)

```julia
function f(a, b=0; extra=0)
  return a + b + extra
end

f(123, extra=321)
```

- <i class="twa twa-light-bulb"></i> Managing arguments en masse

```julia
euclidean(x; kwargs...) = sqrt.(sum(x.^2; kwargs...))

max_squared(args...) = maximum(args .^ 2)
```



# Broadcasting over iterable things

-   Broadcasting operators by prepending a dot

```julia
matrix[row, :] .+= vector1 .* vector2
```

-   Broadcasting a function

```julia
sqrt.(1:10)
maximum.(eachcol(rand(100,100)))

x = [1,2,3,4]
x' .* x
```

<i class="twa twa-light-bulb"></i> The "magic dot" is a shortcut for calling `broadcast(...)`.



# Advanced container types

- Dictionaries (`Dict{KeyType, ValueType`) allow O(log n) indexing, great for
  lookups or keyed data structures. Contents may be typed for increased
  efficiency.

```julia
person = Dict("name" => "John", "surname" => "Foo", "age" => 30)
person["age"]

indexof(v::Vector) = Dict(v .=> eachindex(v))
```

- <i class="twa twa-light-bulb"></i> Sets are key-only containers (keys are _unique_)

```julia
julia> x=Set([1,2,3,2,1]);
julia> println(x)
Set([2, 3, 1])

julia> push!(x,5);
julia> push!(x,5);
julia> println(x)
Set([5, 2, 3, 1])
```
