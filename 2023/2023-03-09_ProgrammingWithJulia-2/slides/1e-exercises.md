
# <i class="twa twa-pencil"></i> Exercise: write a median function

Approaches (let's do at least 2)
- sort and pick
- quick-median (aka. lightweight sort&pick, as in quicksort)
- approximation-improving-median
  - this parallelizes well
  - at which point is this faster than quick-median?
  - did we lose any property?
    - what about median strings?



# Homework 0
Make a package from the "trivial" median algorithm.

- Use julia's `]generate`.
- Have a look at how unit tests are done
- Make sure this unittest works:

```julia
using MagicMedian

@testset "median funcitonality test" begin
	@test simplemedian([1,2,3,4]) >= 2
	@test simplemedian([1,2,3,4]) <= 3
	@test simplemedian([2,6,8]) == 6
end
```

- Zip the package (or `.tar.gz` it or whatever) and upload it to Moodle

How to run unit tests?
- place unittest code into `test/runtests.jl` in the package file
- run `]test MagicMedian`



# <i class="twa twa-pencil"></i> Exercise: let's play a maze game

- Read a maze from a file (let's have `0`s for a corridor and `1`s for a wall)
- Draw it to console using `.` and `#`, add a border.
- Make a function that annotates the whole maze based on how many (axis-aligned) steps a person (maze inhabitant) needs to take from some point
  - let's use the slow <i class="twa twa-water-wave"></i>wave<i class="twa twa-water-wave"></i> algorithm
- Use `UnicodePlots` to plot interesting stuff
  - shortest path length distribution
  - shortest-path-length heatmap of the maze
  - "most blocking walls" (how much time to reach the father side of the wall could be gained by removing the wall?)



# Exercise: Tricky questions about the maze

What is the slowest part of the "wave" solution?

How can we make it faster?



# Homework 1

Make a *faster* version of the maze distance computation.

- any definiton of "faster solution" works; keep it terse
- recommended way:
  - Use `DataStructures.jl` or any other Julia package to get a priority queue working
  - Implement some simple version of Dijkstra's algorithm
- alternatively, try to optimize our naive array algorithm
- measure your speedup with `@time` on mazes of at least `128*128` tiles
- data for testing will be available (probably on Moodle)

Submission:
- wrap your code into a package `MagicMaze`
- include a function that computes the distances, with a type signature such as `maze_distance_map(maze::Matrix{Bool}, x::Int, y::Int)::Matrix{Int}`
- write a unit test that demonstrates the result
- pack the package and upload it to Moodle
