# Overview

1. Why would you learn another programming language again?
2. `$OTHERLANG` to Julia in 15 minutes
3. Running distributed Julia on ULHPC
4. Easy GPU programming with CUDA.jl
