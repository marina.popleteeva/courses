# Rebasing (1)

* `git rebase` enables to shift forward your commits in time
* Move/combine a sequence of commits to a new base commit
* Avoid discrepancies when multiple people work on the same project
* Linear git history (no merge commits)
* Rebasing is like saying, “I want to base my changes on what everybody has already done.”

Imagine the following situation:
<div style="top: 14em; left: 25%; position: absolute;">
    <img src="slides/img/beforeRebase.png" height="500px">
</div>

* There are commits on `develop` that aren't in `myBranch`.



# Rebasing (2)

* After rebase, the commits in the `myBranch` branch will be place on top of `develop`.

<div style="top: 5em; left: 25%; position: absolute;">
    <img src="slides/img/afterRebase.png" height="500px">
</div>



# Example (1):

* A merge request against `develop` is still open. **Repository maintainer: review, and merge it.**

* Create a file in your branch `myBranch`
```bash
$ git checkout myBranch # if necessary
$ echo "# List of attendees" > list.md
$ # add and commit the file
```

* Then, update your `develop` branch from the `upstream` remote:
```bash
$ git fetch upstream
$ git checkout develop
$ git merge upstream/develop
$ git checkout myBranch
```

* Check the histories of both branches
```bash
$ git log
```



# Example (2):

* Rebase `myBranch` on top of the updated `develop`:
```bash
$ git checkout myBranch
$ git rebase develop
```

* Check the history of your branch again
```bash
$ git log
```

* If you pushed previously your branch `myBranch`, you need to rewrite its history remotely - you need to **force push**.



# Interactive Rebasing - flag `-i`

* An interactive rebase is performed with the `-i` flag:

```bash
git rebase -i <branch>
```

* Enables more precise control over the rebased commits
* Before committing many actions are available

```bash
#  p, pick = use commit
#  r, reword = use commit, but edit the commit message
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#  f, fixup = like "squash", but discard this commit's log message
#  x, exec = run command (the rest of the line) using shell
```



# Example 1: Reword and fixup (1)

* Switch to your own branch `myBranch`
* Add and commit two files to this branch:

```bash
$ # git checkout myBranch && cd attendees
$ echo "# William Odell" > william.md
$ # add and commit the file william.md with the message 'add william to attendee list'
$ echo "# Roberta Ross" > roberta.md
$ # add and commit the file roberta.md with the message 'add roberta to attendee list'
$ git push origin myBranch
```

Now, we want to:

- Reword the first commit's message to: `Add William and Roberta to attendee list`

- Combine the second and first commit into one

- Omit the commit message of the second commit.




# Example 1: Reword and fixup (2)

* Perform an interactive rebase with the two last commits:

```bash
$ git rebase -i HEAD~2
```

* The dialog shows up (example):
```bash
$ pick 1234567 add william to attendee list
$ pick abcdef0 add roberta to attendee list
```

* The keywords `pick` can now be changed to `reword` and `fixup` respectively:
```bash
$ reword 1234567 add william to attendee list
$ fixup abcdef0 add roberta to attendee list
```
* Edit by typing `i`
* Change the message of commit `1234567` to `Add William and Roberta to the attendee list`
* Save with `:wq`



# Example 2: Pick and squash (2)

* If you want to **keep** both commit messages in one commit, change the action to `squash`
```bash
$ pick 1234567 add william to attendee list
$ squash abcdef0 add roberta to attendee list
```

* This will create a commit with both modified files, with the commit message being a combination of the two commit messages.

* Push the changes to `myBranch` with `-f`:
```bash
$ git push origin myBranch -f
```