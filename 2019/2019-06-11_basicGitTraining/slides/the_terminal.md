# PART II

<br><br><h1>Basic git course</h1>



# The terminal (shell)

**macOS users:**

Start the Terminal from your `/Applications` directoy.

![bulb](slides/img/bulb.png) Install iTerm2: `https://www.iterm2.com`

**Windows users:**

Install Git Bash: <br>`https://git-scm.com/download/win`

**Linux users:**

Launch default terminal.<br>
![bulb](slides/img/bulb.png) Install Terminator: `https://launchpad.net/terminator`



# First steps in the terminal

Starting the terminal presents itself with a line where you can enter a command
```bash
cesar@myComputer>
```

Often written, for covenience, as
```bash
$
```

When you open your terminal (shell), you are located
in your home directory (unless otherwise configured), denoted as `~/`.



# Essential Linux commands

List the contents of a directory
```bash
$ ls #-lash
```

Create a directory
```bash
$ mkdir myNewDirectory
```

Change the directory to a specific folder
```bash
$ cd myNewDirectory
```

Change the directory 1 level and 2 levels up
```bash
$ cd ..
# 1 level up

$ cd ../..
# 2 levels up
```



Move a file or a directory
```bash
$ mv myFile.m myNewDirectory/.
```


Rename a file or a directory
```bash
$ mv myFile.m myNewFile.m
$ mv myNewDirectory myDirectory
```