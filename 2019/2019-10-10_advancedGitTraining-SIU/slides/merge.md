# Merging branches locally

* Merge a branch into another one locally
* Combines all the commits from a source branch onto a target branch
* In practice, this is very useful if you 'just want to try out something', or 'draft' something



# Example (1)

* Create a new branch from your `myBranch` branch:
```bash
$ git checkout myBranch
$ git checkout -b myNewBranch
```

* Add two files to the `myNewBranch` branch in two separate commits:
```bash
$ echo "# Trevor Westman" > trevor.md
$ # add and commit the file trevor.md
$ echo "# Gustav Bergen" > gustav.md
$ # add and commit the file gustav.md
```



# Example (2)

* Check the `log` of the `myNewBranch` and `myBranch` branches:
```bash
$ git log myBranch
$ git log myNewBranch
```

* Go to `myBranch` and merge the `myNewBranch` branch into it
```bash
$ git checkout myBranch
$ git merge myNewBranch
$ git log myBranch
```
