# Advanced Git Training - SIU

## October 10, 2019

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br>
    <h1>Advanced Git Concepts</h1>
    <br><br><br>
    <h2>
        Laurent Heirendt, PhD<br><br>
        laurent.heirendt@uni.lu<br><br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h2>
</div>
