# How do I start working on a repository?

You have to `clone` it first:

```bash
$ git clone ssh://git@git-r3lab-server.uni.lu:8022/<groupName>/myRepo.git myRepo
```

If you did not configure your SSH key, clone using HTTPS:
```bash
$ git clone https://git-r3lab.uni.lu/<groupName>/myRepo.git myRepo
```

You will be prompted to enter your credentials.
