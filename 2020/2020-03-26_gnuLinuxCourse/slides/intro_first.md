# Welcome to the Inaugural R3 GNU/Linux Course! 

* Thank you to the [R3 team][1] for the opportunity to introduce you to [GNU][2] and [Linux][3]

<!-- .element: class="fragment" -->
* This is an extraordinary situation, so lets keep things flexible

<!-- .element: class="fragment" -->
* `echo $(whoami)`
  + Researcher on the [Environmental Cheminformatics][4] team
  
  <!-- .element: class="fragment" -->
  + GNU/Linux user since [Debian Woody][5] (ca 2002)
	+ Arrived as a seven CD set from a small computer shop from the
      list of international vendors at [Getting Debian][6]
	
  <!-- .element: class="fragment" -->

<!-- .element: class="fragment" -->


[1]: https://wwwfr.uni.lu/lcsb/research/bioinformatics_core/responsible_and_reproducible_research_r3
[2]: https://www.gnu.org
[3]: https://linux.org
[4]: https://wwwen.uni.lu/lcsb/research/environmental_cheminformatics
[5]: https://www.debian.org/releases/woody/
[6]: https://www.debian.org/CD/vendors/


