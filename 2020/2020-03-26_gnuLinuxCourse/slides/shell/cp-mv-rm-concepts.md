# File Explorer: Copy(cp), Move(mv) and Remove(rm) (Concepts)

* cp, mv and rm can act on either files, or directories
  ```shell
	  cp file1 file2
	  mv file1 file2
	  
	  cp -R dir1 new/location
	  mv dir1 new/location
	  
	  cp file1 file2 ... some/existing/dir
	  mv file1 file2 ... some/existing/dir
	  
	  rm file1 ...
	  rm -R dir
	  rm -Rf dir # Danger!
  ```
  
* Effectiveness of these commands greatly improved by file patterns
  - *, ., [0-9],[ABC], {}, etc
