# Accessing Computers over Network (Securely)

* ... programs have bugs

<!-- .element: class="fragment" -->
* Bugs in communicating program enable [backdoors][1] and other [exploits][2]

<!-- .element: class="fragment" -->
* A computer on a public networks with ports unprotected is a recipe for disaster

<!-- .element: class="fragment" -->
* Which is why a computer from a previous point is never exposed to public
	... unless it's a [honeypot][3]

	<!-- .element: class="fragment" -->
	
<!-- .element: class="fragment" -->

[1]: https://www.techrepublic.com/article/is-the-intel-management-engine-a-backdoor/
[2]: https://en.wikipedia.org/wiki/Heartbleed
[3]: https://en.wikipedia.org/wiki/Honeypot_(computing)
