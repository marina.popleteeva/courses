# Responsible and Reproducible Research (R<sup>3</sup>)
## What is R<sup>3</sup>?


A multi-facetted change management
process built on 3 pillars:

- R3 pathfinder

- R3 school

- R3 accelerator

Common link module: R3 clinic

<div style="top: -1em; left: 50%; position: absolute;">
    <img src="slides/img/3pillars-full.png">
</div>
  
<br>
<br>
<br>
<br>

<aside class="notes">
Pathfinder - policies, finding optimal data management changes<br>
School - courses, howtos, trainnings<br>
Accelerator - advanced teams and their boost/support, CI/CD setup<br>
Clinic - hands-on, meetings in groups, code review + suggestions<br>
</aside>

## R<sup>3</sup> Training
  * LCSB's Monthly Data Management and Data Protection training
  * ELIXIR Luxembourg's trainings <br>
   https://elixir-luxembourg.org/training
  * R<sup>3</sup> school Git basics - every 4 months 
  <aside class="notes">
    Direct newcommers to this monthly training  
  </aside>



# Responsible and Reproducible Research (R<sup>3</sup>)
<section data-transition="none" data-background-image="slides/img/r3-training-logo.png" data-background-size="1000px" data-background-opacity="0.1">
</section>
<div style="display:block;text-align:center;position:relative;" >
<div class="profile-container">

  * Reinhard Schneider
  * <img src="slides/img/R3_profile_pictures/reinhard_schneider.png">
  * Head of Bioinformatics Core

</div>
<div class="profile-container">

  * Pinar Alper
  * <img src="slides/img/R3_profile_pictures/pinar_alper.png">
  * Datasteward

</div>
<div class="profile-container">

  * Yohan Yarosz</li>
  * <img src="slides/img/R3_profile_pictures/yohan_yarosz.png">
  * Development

</div>
<div class="profile-container">

  * Laurent Heirendt</li>
  * <img src="slides/img/R3_profile_pictures/laurent_heirendt.png">
  * Git, CI

</div>
<div class="profile-container">

  * Sarah Peter</li>
  * <img src="slides/img/R3_profile_pictures/sarah_peter.png">
  * Infrastructure

</div>
<div class="profile-container">

  * Valentine Groues</li>
  * <img src="slides/img/R3_profile_pictures/valentine_groues.png">
  * Development
  
</div>
<div class="profile-container">

  * Vilem Ded</li>
  * <img src="slides/img/R3_profile_pictures/vilem_ded.png">
  * Datasteward
  
</div>
<div class="profile-container">

  * Noua Toukourou</li>
  * <img src="slides/img/R3_profile_pictures/noua_toukourou.png">
  * Infrastructure
  
</div>
<div class="profile-container">

  * Alexey Kolodkin</li>
  * <img src="slides/img/R3_profile_pictures/alexey_kolodkin.png">
  * Datasteward
  
</div>
<div class="profile-container">

  * Maharshi Vyas</li>
  * <img src="slides/img/R3_profile_pictures/maharshi_vyas.png">
  * Infrastructure
  
</div>

<div class="profile-container">

  * Nene Barry</li>
  * <img src="slides/img/R3_profile_pictures/nene_barry.png">
  * Datasteward
  
</div>

<div class="profile-container">

  * Karim Chaouch</li>
  * <img src="slides/img/R3_profile_pictures/karim_chaouch.png">
  * Development
  
</div>
<div class="profile-container">

  * Christophe Trefois
  * <img src="slides/img/R3_profile_pictures/christophe_trefois.png">
  * R<sup>3</sup> team lead

</div>
</div>
