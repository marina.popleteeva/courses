# The editor(s)

Recommended editors:

- **Visual Studio Code** [https://code.visualstudio.com](https://code.visualstudio.com)
- **Atom** [https://atom.io](https://atom.io)

*Note*: Other editors can, of course, also be used. Examples:
- IntelliJ IDEA [https://www.jetbrains.com/idea](https://www.jetbrains.com/idea)
- Sublime Text [https://www.sublimetext.com](https://www.sublimetext.com)

<img src="slides/img/icon-live-demo.png" height="100px">
