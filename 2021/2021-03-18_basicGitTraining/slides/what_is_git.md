# What is `git`?

<!-- ![](slides/img/git_definition.png) -->

`git` is a **version control system** (VCS) for tracking changes in computer files and coordinating work on those files among multiple people [1].

Designed and implemented in 2005 by **Linus Torvalds**

<div align="center">
<img src="slides/img/linus.jpg">
</div>

[1] *https://en.wikipedia.org/wiki/Git*



# The inventor of `git`

<div align="center">
<img src="slides/img/git_definition.png">
</div>

`I'm an egotistical bastard, and I name all my projects after myself.
First Linux, now git.`
Linus Torvald (2007-06-14)



# What is the use of `git`?

* No need to fully rewrite code; **reuse code** and **save time**
* Keep the changes you made over time (**history**)
* Allows you to **backtrack** (if necessary) and undo unwanted changes
* Easily **add contributions** of your collaborators to the main code base

note:

Other points to mention:
* git shall not be considered as a nuisance, but as a tool that should help to track and trace the code.
* git is not to track performance. Not using it shows exactly the opposite.
